import 'package:flutter/material.dart';


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Form nhập liệu",
        home: Scaffold(
          appBar: AppBar(title: Text('Form nhập thông tin')),
          body: FormScreen(),
        )
    );
  }

}

class FormScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<StatefulWidget>{
  final formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20),),
            fieldPassword(),
            Container(margin: EdgeInsets.only(top: 20),),
            Container(margin: EdgeInsets.only(top: 20),),
            submitButton()
          ],
        ),
      ),
    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.email),
          labelText: 'Địa chỉ Email'
      ),
      validator: (value) {
        if (!value!.contains(RegExp(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z])+'))) {
          return 'Vui lòng nhập địa chỉ email hợp lệ!';
        }
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      decoration: InputDecoration(
          icon: Icon(Icons.password),
          labelText: 'Mật khẩu đăng nhập'
      ),
      validator: (value) {
        if (value!.isEmpty) {
          return 'Hãy nhập mật khẩu';
        } else if (!value.contains(RegExp(r'[A-Z]'))) {
          return 'Mật khẩu phải có ít nhất một ký tự hoa';
        } else if (!value.contains(RegExp(r'[a-z]'))) {
          return 'Mật khẩu phải có ít nhất một ký tự thường';
        } else if (!value.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
          return 'Mật khẩu phải có ít nhất một ký tự đặc biệt';
        } else if (value.length < 8 ) {
          return 'Mật khẩu phải có ít nhất 8 kí tự';
        }
        return null;
      },
    );
  }




  Widget submitButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            print('Submit successfully!');
          }
        },
        child: Text('Gửi'));
  }
}
